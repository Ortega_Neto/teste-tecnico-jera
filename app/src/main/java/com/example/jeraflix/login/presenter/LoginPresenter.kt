package com.example.jeraflix.login.presenter

import android.content.Context
import com.example.jeraflix.login.interactor.ILoginPresenter
import com.example.jeraflix.login.view.LoginActivity
import com.example.jeraflix.utils.VariaveisGlobais.Companion.CONTA_DE_USUARIO
import com.example.jeraflix.utils.databases.contaDatabase.ContaController
import com.example.jeraflix.utils.dialogs.LoadingDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginPresenter(val context: Context): ILoginPresenter {
    private val loginActivity = context as LoginActivity
    private val contaController =
        ContaController(
            context
        )

    override fun verificarEmailESenha(email: String, senha: String){
        val dialog = LoadingDialog("Realizando Login...", context)
        dialog.mostrar()

        contaController.buscarContaPorEmail(email){ erro, conta ->
            when {
                erro != null -> {
                    loginActivity.erroAoRealizarLogin(erro)
                    CoroutineScope(Dispatchers.Main).launch {
                        dialog.sumir()
                    }
                }
                conta != null -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        CONTA_DE_USUARIO = conta
                        dialog.sumir()
                        if (senha == conta.senha) {
                            loginActivity.irParaAHome()
                        } else {
                            loginActivity.erroAoRealizarLogin("Erro ao realizar login: Senha incorreta")
                        }
                    }
                }
                else -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        dialog.sumir()
                        loginActivity.erroAoRealizarLogin("Erro ao realizar login: Conta não existente")
                    }
                }
            }
        }
    }

}