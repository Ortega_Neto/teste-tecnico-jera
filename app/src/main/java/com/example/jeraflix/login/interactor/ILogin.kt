package com.example.jeraflix.login.interactor

import android.view.View

interface ILogin {
    fun realizarLogin(view: View)
    fun erroAoRealizarLogin(erro: String)
    fun irParaAHome()
    fun criarNovaConta(view: View)
}