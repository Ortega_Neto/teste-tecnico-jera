package com.example.jeraflix.login.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.jeraflix.R
import com.example.jeraflix.criarConta.view.CriarContaActivity
import com.example.jeraflix.home.view.HomeActivity
import com.example.jeraflix.login.interactor.ILogin
import com.example.jeraflix.login.presenter.LoginPresenter
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppCompatActivity(), ILogin {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    override fun realizarLogin(view: View){
        if(edtLoginEmail.text.isNotEmpty() || edtLoginSenha.text.isNotEmpty()){
            LoginPresenter(this@LoginActivity).verificarEmailESenha(
                edtLoginEmail.text.toString(),
                edtLoginSenha.text.toString()
            )
        }
        else{
            if(edtLoginEmail.text.isEmpty()) edtLoginEmail.error = getString(R.string.login_email)
            if(edtLoginSenha.text.isEmpty()) edtLoginSenha.error = getString(R.string.login_senha)
        }
    }

    override fun erroAoRealizarLogin(erro: String){
        Toast.makeText(
            this@LoginActivity,
            erro,
            Toast.LENGTH_LONG
        ).show()
    }

    override fun irParaAHome(){
        edtLoginEmail.text.clear()
        edtLoginSenha.text.clear()

        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun criarNovaConta(view: View){
        val intent = Intent(this@LoginActivity, CriarContaActivity::class.java)
        startActivity(intent)
    }
}