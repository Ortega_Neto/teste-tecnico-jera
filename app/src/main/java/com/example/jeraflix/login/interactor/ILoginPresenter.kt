package com.example.jeraflix.login.interactor

interface ILoginPresenter {
    fun verificarEmailESenha(email: String, senha: String)
}