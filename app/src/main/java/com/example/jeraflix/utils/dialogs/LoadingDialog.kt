package com.example.jeraflix.utils.dialogs

import android.content.Context
import android.graphics.Color
import cn.pedant.SweetAlert.SweetAlertDialog

data class LoadingDialog(
    var mensagem: String,
    val context: Context,
    var pDialog: SweetAlertDialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
){
    fun atualizarTexto(novaMensagem: String){
        pDialog.titleText = novaMensagem
    }

    fun mostrar(cancelavel: Boolean = false){
        pDialog.progressHelper.barColor = Color.parseColor("#008577")
        pDialog.titleText = this.mensagem
        pDialog.setCancelable(cancelavel)
        pDialog.show()
    }

    fun sumir(){
        pDialog.cancel()
    }
}