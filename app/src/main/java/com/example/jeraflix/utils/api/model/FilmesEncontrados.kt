package com.example.jeraflix.utils.api.model

import com.google.gson.annotations.SerializedName

data class FilmesEncontrados(
    @SerializedName("popularity") val popularidade: Double,
    @SerializedName("vote_count") val contadorDeVotos: Int,
    @SerializedName("poster_path") val pathDoPoster: String,
    @SerializedName("id") val idDoFilme: Int,
    @SerializedName("adult") val filmeAdulto: Boolean,
    @SerializedName("original_language") val linguaguemOriginal: String,
    @SerializedName("original_title") val tituloOriginal: String,
    @SerializedName("genre_ids") val idsDosGeneros: List<Int>,
    @SerializedName("title") val tituloEmPortugues: String,
    @SerializedName("vote_average") val notaMedia: Float,
    @SerializedName("overview") val sinopseDoFilme: String,
    @SerializedName("release_date") val dataDeLancamento: String
)