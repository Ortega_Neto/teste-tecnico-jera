package com.example.jeraflix.utils.api

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DadosDaApi {
    companion object{
        val BASE_URL: String = "https://api.themoviedb.org/3/"
        val CHAVE_DA_API: String = "d65e57fb832989a7c7889198d3dded9d"
        val LINGUAGEM: String = "pt-BR"
        val INCLUIR_FILMES_ADULTOS: String = "true"
        val NAO_INCLUIR_FILMES_ADULTOS: String = "false"

        fun carregarChamadaDeApi(
            onFailure: (Throwable) -> Unit,
            block: suspend () -> Unit
        ): Job {
            return CoroutineScope(Dispatchers.IO).launch {
                try {
                    block()
                } catch (error: Throwable) {
                    onFailure(error)
                }
            }
        }

    }
}