package com.example.jeraflix.utils.api.interactor

import com.example.jeraflix.utils.api.model.FilmesEncontrados

interface IChamadasDeApi {
    suspend fun buscarFilmes(busca: String, callback: (erro: String?, filmes: List<FilmesEncontrados>?) -> Unit)
}