package com.example.jeraflix.utils.api.interactor

import com.example.jeraflix.utils.api.model.FilmesDaBusca
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiHelper {
    @GET("search/movie?")
    suspend fun buscarFilmes(
        @Query("api_key") chave: String,
        @Query("language") linguagem: String,
        @Query("page") pagina: String,
        @Query("include_adult") adulto: String,
        @Query("query") query: String
    ): Response<FilmesDaBusca>
}