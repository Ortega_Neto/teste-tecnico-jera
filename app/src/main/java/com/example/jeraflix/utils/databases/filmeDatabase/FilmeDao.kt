package com.example.jeraflix.utils.databases.filmeDatabase

import androidx.room.*

@Dao
interface FilmeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun inserirFilme(filme: Filme)

    @Query("SELECT * FROM filmes")
    fun buscarTodosOsFilmes(): List<Filme>
}