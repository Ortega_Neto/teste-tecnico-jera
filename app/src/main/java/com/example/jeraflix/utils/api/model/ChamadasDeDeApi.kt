package com.example.jeraflix.utils.api.model

import com.example.jeraflix.utils.api.interactor.IChamadasDeApi

class ChamadasDeDeApi: IChamadasDeApi{
    override suspend fun buscarFilmes(busca: String, callback: (erro: String?, filmes: List<FilmesEncontrados>?) -> Unit){
        val resposta = InstanciarChamadasDeApi().buscarFilmes(busca)

        if(resposta.isSuccessful){
            callback(null, resposta.body()!!.filmes)
        }
        else{
            callback("Erro ao buscar filmes", null)
        }
    }
}