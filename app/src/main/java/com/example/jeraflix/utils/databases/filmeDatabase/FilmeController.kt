package com.example.jeraflix.utils.databases.filmeDatabase

import android.content.Context
import android.widget.Toast
import com.example.jeraflix.utils.databases.ContaDataBase
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main

class FilmeController(val context: Context) {
    private val database = ContaDataBase.getInstance(context)
    private var filmes: List<Filme> = emptyList()

    fun inserirFilme(filme: Filme, callback: (erro: String?) -> Unit){
        CoroutineScope(Default).launch {
            try {
                database.filmeDao().inserirFilme(filme)
                withContext(Main){
                    Toast.makeText(context, "Filme salvo na lista de assistir!", Toast.LENGTH_LONG).show()
                }
                callback(null)
            }
            catch (e: Exception){
                withContext(Main){
                    Toast.makeText(context, "Não foi possível salvar este filme", Toast.LENGTH_LONG).show()
                }
                callback(e.toString())
            }
        }
    }

    fun buscarFilmesPorEmail(email: String, callback: (erro: String?, filmes: List<Filme>?) -> Unit){
        var erro: String? = null
        var filmesDoEmail: List<Filme> = emptyList()

        CoroutineScope(Default).launch {
            try{
                coroutinebuscarFilmesPorEmail().await()
                filmes.forEach {
                    if (it.emailDaConta == email) {
                        filmesDoEmail = filmesDoEmail + it
                    }
                }
            }
            catch(error: Throwable){
                withContext(Main) {
                    erro= "Erro ao buscar filmes"
                }
            }
            finally {
                callback(erro, filmesDoEmail)
            }
        }
    }

    private fun coroutinebuscarFilmesPorEmail() = GlobalScope.async{
        filmes = database.filmeDao().buscarTodosOsFilmes()
    }
}