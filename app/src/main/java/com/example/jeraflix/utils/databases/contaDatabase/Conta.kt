package com.example.jeraflix.utils.databases.contaDatabase

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contas")
data class Conta(
        @PrimaryKey
        var email: String,
        var nome: String,
        var dataDeNascimento: String,
        var senha: String
)