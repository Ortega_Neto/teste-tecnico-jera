package com.example.jeraflix.utils.databases

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.example.jeraflix.utils.databases.contaDatabase.Conta
import com.example.jeraflix.utils.databases.contaDatabase.ContaDao
import com.example.jeraflix.utils.databases.filmeDatabase.Filme
import com.example.jeraflix.utils.databases.filmeDatabase.FilmeDao

private const val DATABASE = "contas"

@Database(entities = [Conta::class, Filme::class], version = 1, exportSchema = false)
abstract class ContaDataBase: RoomDatabase(){
    abstract fun contaDao(): ContaDao
    abstract fun filmeDao(): FilmeDao

    companion object{
        @Volatile
        private var instance: ContaDataBase? = null

        fun getInstance(context: Context): ContaDataBase {
            return instance
                ?: synchronized(this) {
                instance
                        ?: buildDatabase(
                            context
                        )
                            .also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): ContaDataBase {
            return Room.databaseBuilder(
                context,
                ContaDataBase::class.java,
                DATABASE
            ).build()
        }
    }
}