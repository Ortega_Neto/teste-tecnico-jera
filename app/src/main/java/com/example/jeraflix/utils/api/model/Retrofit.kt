package com.example.jeraflix.utils.api.model

import com.example.jeraflix.utils.api.interactor.ApiHelper
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Retrofit {
    fun criarRetrofit(url: String): ApiHelper?{
        return getRetrofitInstance(url).create(ApiHelper::class.java)
    }

    fun getRetrofitInstance(path: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(path)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttp.build())
            .build()
    }

    private val okHttp = OkHttpClient.Builder()
        .connectTimeout(60000, TimeUnit.MILLISECONDS)
        .callTimeout(60000, TimeUnit.MILLISECONDS)
        .writeTimeout(60000, TimeUnit.MILLISECONDS)
        .readTimeout(60000, TimeUnit.MILLISECONDS)
}