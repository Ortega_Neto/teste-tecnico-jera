package com.example.jeraflix.utils.api.model

import com.google.gson.annotations.SerializedName

data class FilmesDaBusca(
    @SerializedName("page") val pagina: Int,
    @SerializedName("total_results") val totalDeFilmesEncontrados: Int,
    @SerializedName("total_pages") val totalDePaginas: Int,
    @SerializedName("results") val filmes: List<FilmesEncontrados>
)