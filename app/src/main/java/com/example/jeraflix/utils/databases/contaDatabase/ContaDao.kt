package com.example.jeraflix.utils.databases.contaDatabase

import androidx.room.*

@Dao
interface ContaDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun inserirConta(conta: Conta)

    @Query("SELECT * FROM Contas")
    fun buscarConta(): List<Conta>

}