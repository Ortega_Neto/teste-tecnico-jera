package com.example.jeraflix.utils.databases.contaDatabase

import android.content.Context
import android.widget.Toast
import com.example.jeraflix.utils.databases.ContaDataBase
import java.lang.Exception
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main

class ContaController(val context: Context){
    private val database = ContaDataBase.getInstance(context)
    private var contas: List<Conta> = emptyList()

    fun inserirConta(conta: Conta){
        CoroutineScope(Default).launch {
            try {
                database.contaDao().inserirConta(conta)
                withContext(Main){
                    Toast.makeText(context, "Conta criada com sucesso!", Toast.LENGTH_LONG).show()
                }
            }
            catch (e: Exception){
                withContext(Main){
                    Toast.makeText(context, "Não foi possível criar a conta", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun buscarContaPorEmail(email: String, callback: (erro: String?, conta: Conta?) -> Unit){
        var erro: String? = null
        var conta: Conta? = null

        CoroutineScope(Default).launch {
            try{
                coroutineBuscarConta().await()
                contas.forEach {
                    if (it.email == email) {
                        conta = it
                    }
                }
            }
            catch(error: Throwable){
                withContext(Main) {
                    erro= "Erro ao buscar contas"
                }
            }
            finally {
                callback(erro, conta)
            }
        }
    }

    private fun coroutineBuscarConta() = GlobalScope.async{
        contas = database.contaDao().buscarConta()
    }

}