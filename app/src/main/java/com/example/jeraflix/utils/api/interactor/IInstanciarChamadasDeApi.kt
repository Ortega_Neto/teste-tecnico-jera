package com.example.jeraflix.utils.api.interactor

import com.example.jeraflix.utils.api.model.FilmesDaBusca
import retrofit2.Response

interface IInstanciarChamadasDeApi {
    suspend fun buscarFilmes(busca: String): Response<FilmesDaBusca>
}