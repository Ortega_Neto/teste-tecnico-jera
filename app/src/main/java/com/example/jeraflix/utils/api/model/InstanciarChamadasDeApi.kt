package com.example.jeraflix.utils.api.model

import com.example.jeraflix.utils.api.DadosDaApi.Companion.BASE_URL
import com.example.jeraflix.utils.api.DadosDaApi.Companion.CHAVE_DA_API
import com.example.jeraflix.utils.api.DadosDaApi.Companion.LINGUAGEM
import com.example.jeraflix.utils.api.DadosDaApi.Companion.NAO_INCLUIR_FILMES_ADULTOS
import com.example.jeraflix.utils.api.interactor.IInstanciarChamadasDeApi
import retrofit2.Response

class InstanciarChamadasDeApi: IInstanciarChamadasDeApi {
    override suspend fun buscarFilmes(busca: String): Response<FilmesDaBusca> {
        val retrofit =  Retrofit().criarRetrofit(BASE_URL)
        return retrofit!!.buscarFilmes(CHAVE_DA_API, LINGUAGEM, "1",
                NAO_INCLUIR_FILMES_ADULTOS, busca)
    }
}