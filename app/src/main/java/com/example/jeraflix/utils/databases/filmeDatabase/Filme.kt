package com.example.jeraflix.utils.databases.filmeDatabase

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "filmes")
data class Filme(
    @PrimaryKey(autoGenerate = true)
    var idDoFilmeNoBD: Int,
    var emailDaConta: String,
    var idDoFilmeNaApi: Int,
    var filmeAdulto: Boolean,
    val tituloOriginal: String,
    val tituloEmPortugues: String,
    val notaMedia: Float,
    val sinopseDoFilme: String,
    val dataDeLancamento: String,
    var filmeMarcadoParaAssistir: Boolean = false,
    var filmeMarcadoComoAssistido: Boolean = false
)