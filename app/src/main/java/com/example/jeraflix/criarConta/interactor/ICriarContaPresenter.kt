package com.example.jeraflix.criarConta.interactor

import com.example.jeraflix.utils.databases.contaDatabase.Conta

interface ICriarContaPresenter {
    fun criarConta(conta: Conta)
}