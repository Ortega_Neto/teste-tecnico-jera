package com.example.jeraflix.criarConta.presenter

import android.content.Context
import com.example.jeraflix.R
import com.example.jeraflix.criarConta.interactor.ICriarContaPresenter
import com.example.jeraflix.criarConta.view.CriarContaActivity
import com.example.jeraflix.utils.databases.contaDatabase.Conta
import com.example.jeraflix.utils.databases.contaDatabase.ContaController
import com.example.jeraflix.utils.dialogs.LoadingDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class CriarContaPresenter(val context: Context): ICriarContaPresenter {
    private val criarContaActivity = context as CriarContaActivity
    private val contaController =
        ContaController(
            context
        )

    override fun criarConta(conta: Conta){
        val dialog = LoadingDialog("Criando conta...", context)
        dialog.mostrar()

        contaController.buscarContaPorEmail(conta.email){ erro, resposta ->
            when {
                erro != null -> {
                    criarContaActivity.erroAoCriarConta(erro)
                    CoroutineScope(Main).launch {
                        dialog.sumir()
                    }
                }
                resposta != null -> {
                    CoroutineScope(Main).launch {
                        dialog.sumir()
                        criarContaActivity.erroAoCriarConta(
                            criarContaActivity.getString(R.string.email_registrado)
                        )
                    }
                }
                else -> {
                    contaController.inserirConta(conta)
                    CoroutineScope(Main).launch {
                        dialog.sumir()
                    }
                    criarContaActivity.finish()
                }
            }
        }
    }

}