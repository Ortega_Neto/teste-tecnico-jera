package com.example.jeraflix.criarConta.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.jeraflix.R
import com.example.jeraflix.criarConta.interactor.ICriarConta
import com.example.jeraflix.criarConta.presenter.CriarContaPresenter
import com.example.jeraflix.utils.databases.contaDatabase.Conta
import kotlinx.android.synthetic.main.activity_criar_nova_conta.*

class CriarContaActivity: AppCompatActivity(), ICriarConta {
    private lateinit var criarContaPresenter: CriarContaPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_criar_nova_conta)
        title = getString(R.string.criar_nova_conta)

        criarContaPresenter = CriarContaPresenter(this@CriarContaActivity)
    }

    override fun verificaDadosRecebidos(view: View){
        if(verificaExistemTodosOsDados()){
            criarContaPresenter.criarConta(
                Conta(
                    edtEmail.text.toString(),
                    edtNome.text.toString(),
                    edtNascimento.text.toString(),
                    edtSenha.text.toString()
                )
            )
        }
        else{
            if(edtNome.text.isEmpty()) edtNome.error = getString(R.string.insira_nome)
            if(edtEmail.text.isEmpty()) edtEmail.error = getString(R.string.insira_email)
            if(edtNascimento.text.isNullOrEmpty())
                edtNascimento.error = getString(R.string.insira_nascimento)
            if(edtSenha.text.isEmpty()) edtSenha.error = getString(R.string.insira_senha)
            if(edtSenha.text.toString() != edtConfimaSenha.text.toString())
                edtConfimaSenha.error = getString(R.string.insira_confirma_senha)
        }
    }

    override fun verificaExistemTodosOsDados(): Boolean{
        return when {
            edtNome.text.isEmpty() -> false
            edtEmail.text.isEmpty() -> false
            edtNascimento.text.isNullOrEmpty() -> false
            edtSenha.text.isEmpty() -> false
            edtSenha.text.toString() != edtConfimaSenha.text.toString() -> false
            else -> true
        }
    }

    override fun erroAoCriarConta(erro: String){
        Toast.makeText(
            this@CriarContaActivity,
            erro,
            Toast.LENGTH_LONG
        ).show()
    }

}