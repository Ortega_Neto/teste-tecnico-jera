package com.example.jeraflix.criarConta.interactor

import android.view.View

interface ICriarConta {
    fun verificaDadosRecebidos(view: View)
    fun verificaExistemTodosOsDados(): Boolean
    fun erroAoCriarConta(erro: String)
}