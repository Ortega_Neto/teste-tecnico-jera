package com.example.jeraflix.home.interactor

import android.view.View

interface IHome {
    fun irParaABuscaDeFilmes(view: View)
    fun irParaListaFilmesParaAssistir(view: View)
}