package com.example.jeraflix.home.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.jeraflix.R
import com.example.jeraflix.buscarFilmes.view.BuscarFilmesActivity
import com.example.jeraflix.home.interactor.IHome
import com.example.jeraflix.listarFilmesParaAssistir.view.FilmesParaAssistirActivity
import com.example.jeraflix.utils.VariaveisGlobais.Companion.CONTA_DE_USUARIO
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity: AppCompatActivity(), IHome {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        title = "Olá, ${CONTA_DE_USUARIO.nome}"
    }

    override fun irParaABuscaDeFilmes(view: View){
        if(edtTextoParaBusca.text.isNotEmpty()) {
            val intent = Intent(this@HomeActivity, BuscarFilmesActivity::class.java)
            intent.putExtra("busca", edtTextoParaBusca.text.toString())
            edtTextoParaBusca.text.clear()
            startActivity(intent)
        }
        else edtTextoParaBusca.error = "Digite algo para buscar os filmes"
    }

    override fun irParaListaFilmesParaAssistir(view: View){
        edtTextoParaBusca.text.clear()
        val intent = Intent(this@HomeActivity, FilmesParaAssistirActivity::class.java)
        startActivity(intent)
    }
}