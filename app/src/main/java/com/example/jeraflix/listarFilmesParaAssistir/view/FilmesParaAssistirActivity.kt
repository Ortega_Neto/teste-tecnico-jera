package com.example.jeraflix.listarFilmesParaAssistir.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.jeraflix.R
import com.example.jeraflix.buscarFilmes.model.ListaDeFilmesAdapter
import com.example.jeraflix.listarFilmesParaAssistir.interactor.IFilmesParaAssistir
import com.example.jeraflix.listarFilmesParaAssistir.model.ListaDeFilmesParaAssistirAdapter
import com.example.jeraflix.listarFilmesParaAssistir.presenter.FilmesParaAssistirPresenter
import com.example.jeraflix.utils.VariaveisGlobais.Companion.CONTA_DE_USUARIO
import com.example.jeraflix.utils.databases.filmeDatabase.Filme
import kotlinx.android.synthetic.main.activity_buscar_filmes.*

class FilmesParaAssistirActivity: AppCompatActivity(), IFilmesParaAssistir {
    private lateinit var filmesParaAssistirPresenter: FilmesParaAssistirPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buscar_filmes)
        title = "Filmes marcados para assistir"

        val buscaDoUsuario = intent.getStringExtra("busca")
        filmesParaAssistirPresenter = FilmesParaAssistirPresenter(this@FilmesParaAssistirActivity)
        filmesParaAssistirPresenter.buscarFilmesSalvosParaAssistir(CONTA_DE_USUARIO.email)
    }

    override fun erroAoBuscarFilmesParaAssistir(erro: String){
        Toast.makeText(
            this@FilmesParaAssistirActivity,
            erro,
            Toast.LENGTH_LONG
        ).show()
    }

    override fun listarFilmesParaAssistir(filmes: List<Filme>){
        val listaDeFilmesAdapter = ListaDeFilmesParaAssistirAdapter(this@FilmesParaAssistirActivity)
        listaDeFilmesAdapter.addAll(filmes)
        lstFilmesEcontrados.adapter = listaDeFilmesAdapter
    }
}