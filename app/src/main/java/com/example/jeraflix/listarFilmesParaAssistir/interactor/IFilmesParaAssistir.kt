package com.example.jeraflix.listarFilmesParaAssistir.interactor

import com.example.jeraflix.utils.databases.filmeDatabase.Filme

interface IFilmesParaAssistir {
    fun erroAoBuscarFilmesParaAssistir(erro: String)
    fun listarFilmesParaAssistir(filmes: List<Filme>)
}