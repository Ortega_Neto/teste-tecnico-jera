package com.example.jeraflix.listarFilmesParaAssistir.interactor

interface IFilmesParaAssistirPresenter {
    fun buscarFilmesSalvosParaAssistir(emailDoUsuario: String)
}