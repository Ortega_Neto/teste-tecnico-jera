package com.example.jeraflix.listarFilmesParaAssistir.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.jeraflix.R
import com.example.jeraflix.utils.databases.filmeDatabase.Filme

class ListaDeFilmesParaAssistirAdapter(context: Context) : ArrayAdapter<Filme>(context, 0) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View = convertView ?: LayoutInflater.from(context).inflate(R.layout.list_item_filme, parent, false)

        val filme = getItem(position)
        val txtNomeEmPortugues =  v.findViewById<TextView>(R.id.txtNomeEmPortugues)
        val txtNomeOriginal =  v.findViewById<TextView>(R.id.txtNomeOriginal)
        val txtDataLancamento =  v.findViewById<TextView>(R.id.txtDataLancamento)
        val txtNotaDoFilme =  v.findViewById<TextView>(R.id.txtNotaDoFilme)

        txtNomeEmPortugues.text = filme!!.tituloEmPortugues
        txtNomeOriginal.text = filme.tituloOriginal
        txtDataLancamento.text = filme.dataDeLancamento
        txtNotaDoFilme.text = filme.notaMedia.toString()

        return v
    }
}