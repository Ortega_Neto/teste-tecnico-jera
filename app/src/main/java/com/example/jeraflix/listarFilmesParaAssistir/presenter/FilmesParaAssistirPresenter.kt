package com.example.jeraflix.listarFilmesParaAssistir.presenter

import android.content.Context
import com.example.jeraflix.listarFilmesParaAssistir.interactor.IFilmesParaAssistirPresenter
import com.example.jeraflix.listarFilmesParaAssistir.view.FilmesParaAssistirActivity
import com.example.jeraflix.utils.databases.filmeDatabase.FilmeController
import com.example.jeraflix.utils.dialogs.LoadingDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FilmesParaAssistirPresenter(val context: Context): IFilmesParaAssistirPresenter {
    private lateinit var dialog: LoadingDialog
    private val filmesParaAssistirActivity = context as FilmesParaAssistirActivity

    override fun buscarFilmesSalvosParaAssistir(emailDoUsuario: String){
        CoroutineScope(Dispatchers.Main).launch {
            dialog = LoadingDialog("Salvando filme na lista de assistir mais tarde...", context)
            dialog.mostrar()
        }

        FilmeController(context).buscarFilmesPorEmail(emailDoUsuario){ erro, filmes ->
            when {
                erro != null -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        dialog.sumir()
                        filmesParaAssistirActivity.erroAoBuscarFilmesParaAssistir(erro)
                    }
                }
                filmes != null -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        dialog.sumir()
                        filmesParaAssistirActivity.listarFilmesParaAssistir(filmes)
                    }
                }
                else -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        dialog.sumir()
                        filmesParaAssistirActivity.erroAoBuscarFilmesParaAssistir("Erro ao realizar login: Conta não existente")
                    }
                }
            }
        }
    }
}