package com.example.jeraflix.buscarFilmes.interactor

import com.example.jeraflix.utils.api.model.FilmesEncontrados

interface IBuscarFilmes {
    fun listarFilmesDaPesquisa(filmesEncontradosEncontrados: List<FilmesEncontrados>)
    fun erroAoSalvarFilmesParaAssistir(erro: String)
}