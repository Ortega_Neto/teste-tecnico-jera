package com.example.jeraflix.buscarFilmes.presenter

import android.content.Context
import android.widget.Toast
import com.example.jeraflix.buscarFilmes.interactor.IBuscarFilmesPresenter
import com.example.jeraflix.buscarFilmes.view.BuscarFilmesActivity
import com.example.jeraflix.utils.VariaveisGlobais
import com.example.jeraflix.utils.VariaveisGlobais.Companion.CONTA_DE_USUARIO
import com.example.jeraflix.utils.api.DadosDaApi
import com.example.jeraflix.utils.api.model.ChamadasDeDeApi
import com.example.jeraflix.utils.api.model.FilmesEncontrados
import com.example.jeraflix.utils.databases.filmeDatabase.Filme
import com.example.jeraflix.utils.databases.filmeDatabase.FilmeController
import com.example.jeraflix.utils.dialogs.LoadingDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class BuscarFilmesPresenter(val context: Context): IBuscarFilmesPresenter {
    private lateinit var dialog: LoadingDialog
    private val buscarFilmesActivity = context as BuscarFilmesActivity

    override fun onErroAoBuscarFilmes(e: Throwable){
        CoroutineScope(Dispatchers.IO).launch {
            dialog.sumir()
            Toast.makeText(
                context,
                e.toString(),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun buscarFilmes(buscaDoUsuario: String){
        CoroutineScope(Main).launch{
            dialog = LoadingDialog("Buscando filmes...", context)
            dialog.mostrar()
        }

        DadosDaApi.carregarChamadaDeApi(::onErroAoBuscarFilmes){
            ChamadasDeDeApi().buscarFilmes(buscaDoUsuario){ erro, filmes ->
                when{
                    erro != null -> {
                        onErroAoBuscarFilmes(Exception(erro))
                    }
                    filmes != null -> {
                        CoroutineScope(Main).launch{
                            dialog.sumir()
                            buscarFilmesActivity.listarFilmesDaPesquisa(filmes)
                        }
                    }
                }
            }
        }
    }

    override fun criarClasseFilmeParaSalvar(filmeSelecionado: FilmesEncontrados){
        salvarFilmeComoAssistirMaisTarde(
            Filme(
                0,
                VariaveisGlobais.CONTA_DE_USUARIO.email,
                filmeSelecionado.idDoFilme,
                filmeSelecionado.filmeAdulto,
                filmeSelecionado.tituloOriginal,
                filmeSelecionado.tituloEmPortugues,
                filmeSelecionado.notaMedia,
                filmeSelecionado.sinopseDoFilme,
                filmeSelecionado.dataDeLancamento,
                true
            )
        )
    }

    override fun salvarFilmeComoAssistirMaisTarde(filme: Filme){
        val dialog = LoadingDialog("Salvando filme na lista de assistir mais tarde...", context)
        dialog.mostrar()

        FilmeController(context).buscarFilmesPorEmail(CONTA_DE_USUARIO.email){ erro, filmes ->
            var estaNaLista: Boolean = false
            filmes?.forEach {
                if(it.idDoFilmeNaApi == filme.idDoFilmeNaApi){
                    estaNaLista = true
                }
            }

            if(!estaNaLista){
                FilmeController(context).inserirFilme(filme){ erro ->
                    dialog.sumir()
                }
            }
            else{
                dialog.sumir()
                CoroutineScope(Main).launch {
                    buscarFilmesActivity.erroAoSalvarFilmesParaAssistir("Este filme já foi salvo na sua lista para assistir!")
                }
            }
        }

    }

}