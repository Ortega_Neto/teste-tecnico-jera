package com.example.jeraflix.buscarFilmes.view

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.jeraflix.R
import com.example.jeraflix.buscarFilmes.interactor.IBuscarFilmes
import com.example.jeraflix.buscarFilmes.model.ListaDeFilmesAdapter
import com.example.jeraflix.buscarFilmes.presenter.BuscarFilmesPresenter
import com.example.jeraflix.utils.VariaveisGlobais
import com.example.jeraflix.utils.VariaveisGlobais.Companion.CONTA_DE_USUARIO
import com.example.jeraflix.utils.api.model.FilmesEncontrados
import com.example.jeraflix.utils.databases.filmeDatabase.Filme
import kotlinx.android.synthetic.main.activity_buscar_filmes.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.alert

class BuscarFilmesActivity: AppCompatActivity(), IBuscarFilmes {
    private lateinit var buscarFilmesPresenter: BuscarFilmesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buscar_filmes)
        title = "Filmes encontrados"

        val buscaDoUsuario = intent.getStringExtra("busca")
        buscarFilmesPresenter = BuscarFilmesPresenter(this@BuscarFilmesActivity)
        buscarFilmesPresenter.buscarFilmes(buscaDoUsuario)
    }

    override fun listarFilmesDaPesquisa(filmesEncontradosEncontrados: List<FilmesEncontrados>){
        if (filmesEncontradosEncontrados.isNotEmpty()) {
            val listaDeFilmesAdapter = ListaDeFilmesAdapter(this@BuscarFilmesActivity)
            listaDeFilmesAdapter.addAll(filmesEncontradosEncontrados)
            lstFilmesEcontrados.adapter = listaDeFilmesAdapter

            lstFilmesEcontrados.setOnItemClickListener { adapterView: AdapterView<*>, view: View, position: Int, id: Long ->
                val filmeSelecionado = listaDeFilmesAdapter.getItem(position)

                alert("Deseja salvar esse filme em qual lista?") {
                    title = "Alerta"
                    positiveButton("Assistir mais tarde") {
                        buscarFilmesPresenter.criarClasseFilmeParaSalvar(filmeSelecionado!!)
                    }
                }.show()
            }
        }
        else{
            erroAoSalvarFilmesParaAssistir("Nenhum filme encontrado")
            finish()
        }
    }

    override fun erroAoSalvarFilmesParaAssistir(erro: String){
        Toast.makeText(
            this@BuscarFilmesActivity,
            erro,
            Toast.LENGTH_LONG
        ).show()
    }
}