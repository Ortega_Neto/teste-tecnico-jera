package com.example.jeraflix.buscarFilmes.interactor

import com.example.jeraflix.utils.api.model.FilmesEncontrados
import com.example.jeraflix.utils.databases.filmeDatabase.Filme

interface IBuscarFilmesPresenter {
    fun onErroAoBuscarFilmes(e: Throwable)
    fun buscarFilmes(buscaDoUsuario: String)
    fun criarClasseFilmeParaSalvar(filmeSelecionado: FilmesEncontrados)
    fun salvarFilmeComoAssistirMaisTarde(filme: Filme)
}